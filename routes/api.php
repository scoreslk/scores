<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
/**
 *  Start Admin Panel Routes
 */
//Route::post('login', 'Admin\AuthController@login');
/**
 *  Login
 */
Route::post('administrator/register', 'Admin\AuthController@register');
Route::post('administrator/login', 'Admin\AuthController@login');
Route::group(['prefix' => 'administrator', 'middleware' => 'auth:api'], function (){

    Route::get('user', 'Admin\AuthController@user');
    Route::post('logout', 'Admin\AuthController@logout');
    /**
     * Divisions
     */
    Route::get('divisions', 'Admin\DivisionController@index');
    Route::get('division/add', 'Admin\DivisionController@add');
    Route::post('division/add', 'Admin\DivisionController@store');
    Route::get('division/edit/{id}', 'Admin\DivisionController@edit');
    Route::post('division/edit/{id}', 'Admin\DivisionController@update');
    Route::post('division/status/{id}/{status}', 'Admin\DivisionController@statusChange');

    /**
     *  Formats
     */
    Route::get('formats', 'Admin\FormatController@index');
    Route::get('format/add', 'Admin\FormatController@add');
    Route::post('format/add', 'Admin\FormatController@store');
    Route::get('format/edit/{id}', 'Admin\FormatController@edit');
    Route::post('format/edit/{id}', 'Admin\FormatController@update');
    Route::post('format/status/{id}/{status}', 'Admin\FormatController@statusChange');

    /**
     *  Grounds
     */
    Route::get('grounds', 'Admin\GroundController@index');
    Route::get('ground/add', 'Admin\GroundController@add');
    Route::post('ground/add', 'Admin\GroundController@store');
    Route::get('ground/edit/{id}', 'Admin\GroundController@edit');
    Route::post('ground/edit/{id}', 'Admin\GroundController@update');
    Route::post('ground/status/{id}/{status}', 'Admin\GroundController@statusChange');

    /**
     *  Level
     */
    Route::get('levels', 'Admin\LevelController@index');
    Route::get('level/add', 'Admin\LevelController@add');
    Route::post('level/add', 'Admin\LevelController@store');
    Route::get('level/edit/{id}', 'Admin\LevelController@edit');
    Route::post('level/edit/{id}', 'Admin\LevelController@update');
    Route::post('level/status/{id}/{status}', 'Admin\LevelController@statusChange');

    /**
     * Umpires
     */
    Route::get('umpires', 'Admin\UmpireController@index');
    Route::get('umpire/add', 'Admin\UmpireController@add');
    Route::post('umpire/add', 'Admin\UmpireController@store');
    Route::get('umpire/edit/{id}', 'Admin\UmpireController@edit');
    Route::post('umpire/edit/{id}', 'Admin\UmpireController@update');
    Route::post('umpire/status/{id}/{status}', 'Admin\UmpireController@statusChange');

    /**
     * Referees
     */
    Route::get('match-referees', 'Admin\MatchRefereeController@index');
    Route::get('match-referee/add', 'Admin\MatchRefereeController@add');
    Route::post('match-referee/add', 'Admin\MatchRefereeController@store');
    Route::get('match-referee/edit/{id}', 'Admin\MatchRefereeController@edit');
    Route::post('match-referee/edit/{id}', 'Admin\MatchRefereeController@update');
    Route::post('match-referee/status/{id}/{status}', 'Admin\MatchRefereeController@statusChange');

    /**
     * Wicket Types
     */
    Route::get('wicket-types', 'Admin\WicketTypeController@index');
    Route::get('wicket-type/add', 'Admin\WicketTypeController@add');
    Route::post('wicket-type/add', 'Admin\WicketTypeController@store');
    Route::get('wicket-type/edit/{id}', 'Admin\WicketTypeController@edit');
    Route::post('wicket-type/edit/{id}', 'Admin\WicketTypeController@update');
    Route::post('wicket-type/status/{id}/{status}', 'Admin\WicketTypeController@statusChange');

    /**
     * Extra Types
     */
    Route::get('extra-types', 'Admin\ExtraTypeController@index');
    Route::get('extra-type/add', 'Admin\ExtraTypeController@add');
    Route::post('extra-type/add', 'Admin\ExtraTypeController@store');
    Route::get('extra-type/edit/{id}', 'Admin\ExtraTypeController@edit');
    Route::post('extra-type/edit/{id}', 'Admin\ExtraTypeController@update');
    Route::post('extra-type/status/{id}/{status}', 'Admin\ExtraTypeController@statusChange');

    /**
     * Schools
     */
    Route::get('schools', 'Admin\SchoolController@index');
    Route::get('school/add', 'Admin\SchoolController@add');
    Route::post('school/add', 'Admin\SchoolController@store');
    Route::get('school/edit/{id}', 'Admin\SchoolController@edit');
    Route::post('school/edit/{id}', 'Admin\SchoolController@update');
    Route::post('school/status/{id}/{status}', 'Admin\SchoolController@statusChange');

    /**
     * Clubs
     */
    Route::get('clubs', 'Admin\ClubController@index');
    Route::get('club/add', 'Admin\ClubController@add');
    Route::post('club/add', 'Admin\ClubController@store');
    Route::get('club/edit/{id}', 'Admin\ClubController@edit');
    Route::post('club/edit/{id}', 'Admin\ClubController@update');
    Route::post('club/status/{id}/{status}', 'Admin\ClubController@statusChange');

    /**
     * Players
     */
    Route::get('players', 'Admin\PlayerController@index');
    Route::get('player/add', 'Admin\PlayerController@add');
    Route::post('player/add', 'Admin\PlayerController@store');
    Route::get('player/edit/{id}', 'Admin\PlayerController@edit');
    Route::post('player/edit/{id}', 'Admin\PlayerController@update');
    Route::post('player/status/{id}/{status}', 'Admin\PlayerController@statusChange');

    /**
     *  Tournaments
     */
    Route::get('tournaments', 'Admin\TournamentController@index');
    Route::get('tournament/add', 'Admin\TournamentController@add');
    Route::post('tournament/add', 'Admin\TournamentController@store');
    Route::get('tournament/edit/{id}', 'Admin\TournamentController@edit');
    Route::post('tournament/edit/{id}', 'Admin\TournamentController@update');
    Route::post('tournament/status/{id}/{status}', 'Admin\TournamentController@statusChange');
});

/**
 *  End Admin Panel Routes
 */
