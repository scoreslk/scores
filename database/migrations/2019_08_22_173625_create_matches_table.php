<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('is_friendly')->default(0);
            $table->integer('tournament_id')->nullable();
            $table->integer('home_team_id');
            $table->integer('away_team_id');
            $table->integer('group_id')->nullable();
            $table->integer('ground_id');
            $table->string('toss', 255)->nullable();
            $table->date('match_dates')->nullable();
            $table->integer('umpire_one_id')->nullable();
            $table->integer('umpire_two_id')->nullable();
            $table->integer('umpire_three_id')->nullable();
            $table->integer('umpire_four_id')->nullable();
            $table->integer('referee_id')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->foreign('tournament_id')
                ->references('id')->on('tournaments')
                ->onDelete('cascade');
            $table->foreign('home_team_id')
                ->references('id')->on('teams')
                ->onDelete('cascade');
            $table->foreign('away_team_id')
                ->references('id')->on('teams')
                ->onDelete('cascade');
            $table->foreign('group_id')
                ->references('id')->on('groups')
                ->onDelete('cascade');
            $table->foreign('ground_id')
                ->references('id')->on('grounds')
                ->onDelete('cascade');
            $table->foreign('umpire_one_id')
                ->references('id')->on('umpires')
                ->onDelete('cascade');
            $table->foreign('umpire_two_id')
                ->references('id')->on('umpires')
                ->onDelete('cascade');
            $table->foreign('umpire_three_id')
                ->references('id')->on('umpires')
                ->onDelete('cascade');
            $table->foreign('umpire_four_id')
                ->references('id')->on('umpires')
                ->onDelete('cascade');
            $table->foreign('referee_id')
                ->references('id')->on('referees')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
