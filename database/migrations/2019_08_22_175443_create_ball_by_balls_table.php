<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBallByBallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ball_by_balls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('match_id');
            $table->integer('innings_id');
            $table->text('comment')->nullable();
            $table->integer('striker_batsman_id');
            $table->integer('none_striker_batsman_id');
            $table->integer('bowler_id');
            $table->integer('runs');
            $table->boolean('is_boundary')->default(0);
            $table->boolean('is_wicket')->default(0);
            $table->integer('wicket_type_id')->nullable();
            $table->integer('out_batsman_id')->nullable();
            $table->integer('fielder_id')->nullable();
            $table->boolean('is_extra')->default(0);
            $table->integer('extra_type_id')->nullable();
            $table->timestamps();

            $table->foreign('striker_batsman_id')
                ->references('id')->on('matches')
                ->onDelete('cascade');
            $table->foreign('none_striker_batsman_id')
                ->references('id')->on('players')
                ->onDelete('cascade');
            $table->foreign('bowler_id')
                ->references('id')->on('players')
                ->onDelete('cascade');
            $table->foreign('out_batsman_id')
                ->references('id')->on('players')
                ->onDelete('cascade');
            $table->foreign('fielder_id')
                ->references('id')->on('players')
                ->onDelete('cascade');
            $table->foreign('out_batsman_id')
                ->references('id')->on('wicket_types')
                ->onDelete('cascade');
            $table->foreign('extra_type_id')
                ->references('id')->on('extra_types')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ball_by_balls');
    }
}
