let scorecard = {
    innings: [
        {
            id: 1,
            scores: [
                {
                    innings_type: 1,
                    team_id: 1,
                    player: [
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                    ]
                },
                {
                    innings_type: 2,
                    team_id: 2,
                    player: [
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                    ]
                }
            ],
            extras: {
                wides: 10,
                no_balls: 1,
                leg_byes: 1,
                byes: 1,
                penalty: 0
            },
            total: 234,
        },
        {
            id: 2,
            scores: [
                {
                    innings_type: 1,
                    team_id: 2,
                    player: [
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                    ]
                },
                {
                    innings_type: 2,
                    team_id: 1,
                    player: [
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                    ]
                }
            ],
            extras: {
                wides: 10,
                no_balls: 1,
                leg_byes: 1,
                byes: 1,
                penalty: 0
            },
            total: 234,
        }
    ],
    target_runs: 245,
    target_overs: 45,
    match_result: "Team 1 won by 45 runs(D/L)",
    winning_team_id: 1,
    losing_team_id: 2,
    is_draw: false,
    is_tie:false,
    is_no_result: false,
    is_abandoned: false,
    is_awarded: false,
    is_conceded: false,
    is_superover: false,
    supper_over: [
        {
            id: 1,
            scores: [
                {
                    innings_type: 1,
                    team_id: 1,
                    player: [
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                    ]
                },
                {
                    innings_type: 2,
                    team_id: 2,
                    player: [
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                    ]
                }
            ],
            extras: {
                wides: 10,
                no_balls: 1,
                leg_byes: 1,
                byes: 1,
                penalty: 0
            },
            total: 234,
        },
        {
            id: 2,
            scores: [
                {
                    innings_type: 1,
                    team_id: 2,
                    player: [
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                        {
                            id: 1,
                            not_out: 0,
                            runs: 45,
                            balls: 75,
                            dots: 0,
                            one: 18,
                            twos: 1,
                            threes: 1,
                            fours: 5,
                            sixes: 0
                        },
                    ]
                },
                {
                    innings_type: 2,
                    team_id: 1,
                    player: [
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                        {
                            id: 21,
                            overs: 6.5,
                            runs: 45,
                            maidens: 0,
                            wicket: 1,
                            dots: 13,
                            fours: 4,
                            sixes: 0
                        },
                    ]
                }
            ],
            extras: {
                wides: 10,
                no_balls: 1,
                leg_byes: 1,
                byes: 1,
                penalty: 0
            },
            total: 234,
        }
    ],
};


