<?php
/**
 * Created by PhpStorm.
 * User: Danuja Fernando
 * Date: 8/27/2019
 * Time: 8:56 PM
 */

namespace App\Repositories;


interface RepositoryInterface
{

    public function all();

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function show($id);
}
