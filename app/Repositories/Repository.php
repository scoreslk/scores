<?php
/**
 * Created by PhpStorm.
 * User: Danuja Fernando
 * Date: 8/27/2019
 * Time: 8:54 PM
 */
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class Repository implements RepositoryInterface
{
    /**
     * model property on class instances.
     * @var Model
     */
    protected $model;

    /**
     * Repository constructor to bind model to repo.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get all instances of model
     * @return \Illuminate\Database\Eloquent\Collection|Model[]
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * create a new record in the database.
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * update record in the database.
     * @param array $data
     * @param $id
     * @return bool
     */
    public function update(array $data, $id)
    {
        $record = $this->model->find($id);
        if($record){
            return $record->update($data);
        }
        return false;
    }


    /**
     * remove record from the database.
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * show the record with the given id
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Set the associated model
     * @param $model
     * @return $this
     */
    public function setModal($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Get the associated model
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Eager load the database relationshops
     * @param $relations
     * @return \Illuminate\Database\Eloquent\Builder|Model
     */
    public function with($relations)
    {
        return $this->model->with($relations);
    }
}
