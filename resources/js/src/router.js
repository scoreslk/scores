/*=========================================================================================
  File Name: router.js
  Description: Routes for vue-router. Lazy loading is enabled.
  Object Strucutre:
                    path => router path
                    name => router name
                    component(lazy loading) => component to load
                    meta : {
                      rule => which user can have access (ACL)
                      breadcrumb => Add breadcrumb to specific page
                      pageTitle => Display title besides breadcrumb
                    }
  ----------------------------------------------------------------------------------------
  Item Name: Vuesax Admin - VueJS Dashboard Admin Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import Router from 'vue-router'
import axios from 'axios'
import Validation from 'vee-validate';

Vue.use(Validation);
Vue.use(Router)
Vue.prototype.$axios = axios;
axios.defaults.baseURL = window.location.origin + '/api/administrator/';
axios.defaults.headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('access_token'),
};
const router = new Router({
    mode: 'history',
    base: '/',
    routes: [

      {
    // =============================================================================
    // MAIN LAYOUT ROUTES
    // =============================================================================
        path: '/',
        component: () => import('./layouts/main/Main.vue'),
        children: [
      // =============================================================================
      // Theme Routes
      // =============================================================================
          {
            path: '/administrator/',
            name: 'home',
            component: () => import('./views/Home.vue'),
          },
          {
            path: '/administrator/page2',
            name: 'page2',
            component: () => import('./views/Page2.vue'),
          },
        ],
      },
    // =============================================================================
    // FULL PAGE LAYOUTS
    // =============================================================================
      {
        path: '/',
        component: () => import('@/layouts/full-page/FullPage.vue'),
        children: [
      // =============================================================================
      // PAGES
      // =============================================================================
          {
            path: '/administrator/login',
            name: 'adminLogin',
            component: () => import('@/views/auth/Login.vue')
          },
          {
            path: '/administrator/error-404',
            name: 'pageError404',
            component: () => import('@/views/pages/Error404.vue')
          },
        ]
      },
      // Redirect to 404 page, if no match found
      {
        path: '*',
        redirect: '/administrator/error-404'
      }
    ],
});

router.beforeEach((to, from, next) => {
    //localStorage.removeItem('access_token');
    let access_token = localStorage.getItem('access_token');
    if(to.name == undefined){
        if(!access_token){
            next({
                name: 'adminLogin'
            });
        }else{
            next({
                name: 'home'
            });
        }
    }
    if(!access_token){
        if(to.name != "adminLogin"){
            next({
                name: 'adminLogin'
            });
        }
    }else{
        if(to.name == "adminLogin"){
            next({
                name: 'home'
            });
        }
    }
    next()
});

export default router
